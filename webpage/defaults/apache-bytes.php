<?php // $Id$

$plugin   = 'apache';
$instance = 'local';
require_once '../rrdgraph.php';

rrdgraph(array( 
    "--title=Apache traffic (Bytes)",
    "--vertical-label=Bytes",

    "DEF :bytes=$path/apache_bytes.rrd:value:AVERAGE",
    "VDEF:total=bytes,TOTAL",

    "AREA:bytes#$c_green:Output",
    'GPRINT:total:Transfered\: %5.1lf%sB\j' 
));

?>
