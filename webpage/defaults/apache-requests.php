<?php // $Id$

$plugin   = 'apache';
$instance = 'local';
require_once '../rrdgraph.php';

rrdgraph(array( 
    "--title=Apache traffic (Requests)",
    "--vertical-label=Requests",
    "--units-exponent=0",

    "DEF:requests=$path/apache_requests.rrd:value:AVERAGE",

    "AREA:requests#$c_blue:Requests",
));

?>
