<?php // $Id$

$plugin   = 'apache';
$instance = 'local';
$type     = 'apache_scoreboard';
require_once '../rrdgraph.php';

list($waiting,   $starting,  $reading, $sending,
     $keepalive, $dnslookup, $closing, $logging,
     $finishing, $idle,      $open
    ) = $rrdgraph_colors['optimum'];


rrdgraph(array( 
    "--title=Scoreboard",
    "--vertical-label=Request",
    '--lower-limit=0',

    "DEF:closing   =$path-closing.rrd     :value:AVERAGE",
    "DEF:dnslookup =$path-dnslookup.rrd   :value:AVERAGE",
    "DEF:finishing =$path-finishing.rrd   :value:AVERAGE",
    "DEF:idle      =$path-idle_cleanup.rrd:value:AVERAGE",
    "DEF:keepalive =$path-keepalive.rrd   :value:AVERAGE",
    "DEF:logging   =$path-logging.rrd     :value:AVERAGE",
    "DEF:open      =$path-open.rrd        :value:AVERAGE",
    "DEF:reading   =$path-reading.rrd     :value:AVERAGE",
    "DEF:sending   =$path-sending.rrd     :value:AVERAGE",
    "DEF:starting  =$path-starting.rrd    :value:AVERAGE",
    "DEF:waiting   =$path-waiting.rrd     :value:AVERAGE",


    "AREA:waiting  #$waiting  :Waiting",
    "AREA:starting #$starting :Starting  :STACK",
    "AREA:reading  #$reading  :Reading   :STACK",
    "AREA:sending  #$sending  :Sending   :STACK",
    "AREA:keepalive#$keepalive:Keepalive :STACK",
    "AREA:dnslookup#$dnslookup:DNS Lookup:STACK",
    "AREA:closing  #$closing  :Closing   :STACK",
    "AREA:logging  #$logging  :Logging   :STACK",
    "AREA:finishing#$finishing:Finishing :STACK",
    "AREA:idle     #$idle     :Idle      :STACK",
    "AREA:open     #$open     :Open      :STACK"

)); 

?>
