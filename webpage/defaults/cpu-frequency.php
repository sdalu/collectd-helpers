<?php

$plugin   = 'aggregation-cpu';
$instance = 'average';
require_once '../rrdgraph.php';

rrdgraph(array( 
    '--title=Speed frequency',
    '--vertical-label=MHz',
    '--lower-limit=0',

    "DEF:freq=$path/cpufreq.rrd:value:AVERAGE",

    "LINE:freq#$c_red:Current",
)); ?>
