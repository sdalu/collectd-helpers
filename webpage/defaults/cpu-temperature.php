<?php

$plugin   = 'aggregation-cpu';
$instance = 'average';
require_once '../rrdgraph.php';

rrdgraph(array( 
    "--title=Temperature",
    "--vertical-label=°C",
    '--upper-limit=50',
    '--lower-limit=20',

    "DEF:cpu_k=$path/temperature.rrd:value:AVERAGE",
    "CDEF:cpu_c=cpu_k,273.2,-",

    "LINE:cpu_c#$c_red:CPU",
)); ?>

