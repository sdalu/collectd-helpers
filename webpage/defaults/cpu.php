<?php

$plugin   = 'aggregation-cpu';
$instance = 'average';
require_once '../rrdgraph.php';

rrdgraph(array( 
    '--title=CPU',
    '--vertical-label=Percent',
    '--upper-limit=100',
    '--lower-limit=0',
    '--rigid',

    "DEF:user     =$path/cpu-user.rrd     :value:AVERAGE",
    "DEF:nice     =$path/cpu-nice.rrd     :value:AVERAGE",
    "DEF:system   =$path/cpu-system.rrd   :value:AVERAGE",
    "DEF:idle     =$path/cpu-idle.rrd     :value:AVERAGE",

    'CDEF:total      =user,nice,system,idle,+,+,+',
    'CDEF:user_p     =100,user,     *,total,/',
    'CDEF:nice_p     =100,nice,     *,total,/',
    'CDEF:system_p   =100,system,   *,total,/',
    'CDEF:idle_p     =100,idle,     *,total,/',

    "AREA:user_p     #$c_yellow :User",
    "AREA:nice_p     #$c_orange :Nice:STACK",
    "AREA:system_p   #$c_red    :System:STACK",
    "AREA:idle_p     #$c_green  :Idle:STACK",
)); ?>
