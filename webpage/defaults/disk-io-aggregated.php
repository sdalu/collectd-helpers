<?php

require_once '../rrdgraph.php';

$t = isset($title) ? $title : "Disk";
$g = array(
    "--title=$t",
    "--vertical-label=Bytes",
    "--lower-limit=0",
);

$reads  = array();
$writes = array();
$plus   = array();
foreach($disks as $d) {
    $g[]= "DEF:{$d}r=$path/disk-{$d}/disk_octets.rrd:read:AVERAGE";
    $g[]= "DEF:{$d}w=$path/disk-{$d}/disk_octets.rrd:write:AVERAGE";
    $reads []= "{$d}r";
    $writes[]= "{$d}w";
    $plus  []= "+";
}
array_shift($plus);

$g[] = "CDEF:read="  . join(',', array_merge($reads, $plus));
$g[] = "CDEF:write=" . join(',', array_merge($writes,$plus));
$g[] = "VDEF:read_total=read,TOTAL";
$g[] = "VDEF:write_total=write,TOTAL";


list($c_read,$c_write) = $colors;
$g[]= "LINE:read#$c_read:Read \g";
$g[]= "GPRINT:read_total: (Transferred\: %5.1lf%sB)\l";
$g[]= "LINE:write#$c_write:Write\g";
$g[]= "GPRINT:write_total: (Transferred\: %5.1lf%sB)\l";


rrdgraph($g);

?>


