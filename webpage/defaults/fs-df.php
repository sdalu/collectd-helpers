<?php

require_once '../rrdgraph.php';

$path = "$hostpath/df-$prefix";

rrdgraph(array( 
    "--title=Space on $prefix",
    '--vertical-label=Bytes',
    '--lower-limit=0',
    '--rigid',

    "DEF:used     =$path/df_complex-used.rrd    :value:AVERAGE",
    "DEF:free     =$path/df_complex-free.rrd    :value:AVERAGE",
    "DEF:reserved =$path/df_complex-reserved.rrd:value:AVERAGE",

    "AREA:used     #$c_red   :Used",
    "AREA:free     #$c_green :Free:STACK",
    "AREA:reserved #$c_blue  :Reserved:STACK",
)); 

?>
