<?php

require_once '../rrdgraph.php';

switch($plugin) {
case 'snmp': $file = "$hostpath/snmp/load.rrd"; break;
default:     $file = "$hostpath/load/load.rrd"; break;
}

rrdgraph(array( 
    "--title=Load",
    "--vertical-label=Load",
    "DEF:short=$file:shortterm:AVERAGE",
    "DEF:mid  =$file:midterm  :AVERAGE",
    "DEF:long =$file:longterm :AVERAGE",

    "LINE:short#$c_blue:Load"
)); ?>
