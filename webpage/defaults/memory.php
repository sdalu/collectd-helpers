<?php

$plugin = 'memory';
require_once '../rrdgraph.php';

$g = array(
    '--title=Memory usage',
    '--vertical-label=Bytes',
    '--lower-limit=0',
    '--base=1024',
);


switch($ostype) {
case 'linux':
    $d = array(
    "DEF:used    =$path/memory-used.rrd    :value:AVERAGE",
    "DEF:cached  =$path/memory-cached.rrd  :value:AVERAGE",
    "DEF:buffered=$path/memory-buffered.rrd:value:AVERAGE",
    "DEF:free    =$path/memory-free.rrd    :value:AVERAGE",

    "AREA:used    #$c_red         :Active",
    "AREA:cached  #$c_yellow      :Cache:STACK",
    "AREA:buffered#$c_green       :Wired:STACK",
    "AREA:free    #$c_olive       :Free:STACK",
    );
    break;

case 'freebsd':
default:
    $d = array(
    "DEF:active  =$path/memory-active.rrd  :value:AVERAGE",
    "DEF:inactive=$path/memory-inactive.rrd:value:AVERAGE",
    "DEF:cache   =$path/memory-cache.rrd   :value:AVERAGE",
    "DEF:wired   =$path/memory-wired.rrd   :value:AVERAGE",
    "DEF:free    =$path/memory-free.rrd    :value:AVERAGE",

    "AREA:active  #$c_red         :Used",
    "AREA:inactive#$c_orange      :Inactive:STACK",
    "AREA:cache   #$c_yellow      :Cache:STACK",
    "AREA:wired   #$c_green       :Wired:STACK",
    "AREA:free    #$c_olive       :Free:STACK",
    );
    break;
}

rrdgraph(array_merge($g,$d));

?>

