<?php 

require_once '../rrdgraph.php';

switch($plugin) {
case 'snmp':
     $file = "$hostpath/snmp/if_octets-$prefix.rrd";
     break;
case 'interface':
default:
     $file = "$hostpath/interface-$prefix/if_octets.rrd";
     break;
}

rrdgraph(array( 
    "--title=Bandwidth usage ($prefix)",
    "--vertical-label=Bytes",
    "--base=1024",

    "DEF :tx=$file:tx:AVERAGE",
    "DEF :rx=$file:rx:AVERAGE",
    "VDEF:tx_total=tx,TOTAL",
    "VDEF:rx_total=rx,TOTAL",

    "LINE:tx#$c_olive:Transmited",
    "GPRINT:tx_total:Transferred\: %5.1lf%sB\j",

    "LINE:rx#$c_blue:Received",
    "GPRINT:rx_total:Transferred\: %5.1lf%sB\j",

)); ?>
