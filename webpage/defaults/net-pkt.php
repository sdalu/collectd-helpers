<?php 

require_once '../rrdgraph.php';

$iface=$prefix;

switch($plugin) {
case 'snmp':
     $f_errors  = "$hostpath/snmp/if_errors-$prefix.rrd";
     $f_packets = "$hostpath/snmp/if_packets-$prefix.rrd";
     break;
case 'interface':
default:
     $f_errors  = "$hostpath/interface-$prefix/if_errors.rrd";
     $f_packets = "$hostpath/interface-$prefix/if_packets.rrd";
     break;
}


rrdgraph(array( 
    "--title=Packets ($prefix)",
    "--vertical-label=Packet",
    "DEF:tx_err=$f_errors :tx:AVERAGE",
    "DEF:rx_err=$f_errors :rx:AVERAGE",
    "DEF:tx_pkt=$f_packets:tx:AVERAGE",
    "DEF:rx_pkt=$f_packets:rx:AVERAGE",

    'CDEF:err=tx_err,rx_err,+',

    "LINE:tx_pkt#$c_olive:Transmited",
    "LINE:rx_pkt#$c_blue :Received",
    "LINE:err   #$c_red  :Error\l",
)); ?>
