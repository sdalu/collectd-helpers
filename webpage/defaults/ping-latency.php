<?php

$plugin="ping";
require_once '../rrdgraph.php';

$t = isset($title) ? $title : "Latency";
$g = array(
    "--title=$t",
    "--vertical-label=ms",
    "--lower-limit=0",
);

$idx = 0;
foreach($latency as $t => $h) {
    $h  = rrdgraph_protect($h);
    $t  = rrdgraph_protect($t);
    $c  = $colors[$idx];
    $g[]= "DEF:l$idx=$path/ping-$h.rrd:value:AVERAGE";
    $g[]= "LINE:l$idx#$c:$t";
    $idx += 1;
}

rrdgraph($g);

?>


