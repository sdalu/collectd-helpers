<?php

$plugin = 'processes';
require_once '../rrdgraph.php';

$g = array(
    "--title=Threads states (sleeping not shown)",
    "--vertical-label=Threads",
    '--lower-limit=0',
    "--units-exponent=0",
);

switch($ostype) {
case 'linux':
    $d = array(
    "DEF:running   =$path/ps_state-running.rrd :value:AVERAGE", 
    "DEF:zombies   =$path/ps_state-zombies.rrd :value:AVERAGE", 
    "DEF:blocked   =$path/ps_state-blocked.rrd :value:AVERAGE", 
    "DEF:stopped   =$path/ps_state-stopped.rrd :value:AVERAGE", 
    "DEF:paging    =$path/ps_state-paging.rrd  :value:AVERAGE", 
    "DEF:sleeping  =$path/ps_state-sleeping.rrd:value:AVERAGE",

    "CDEF:sleep=sleeping",

    "AREA  :running#$c_green  :Running ", 
    "AREA  :zombies#$c_red    :Zombies :STACK", 
    "AREA  :blocked#$c_cyan   :Blocked :STACK", 
    "AREA  :stopped#$c_magenta:Stopped :STACK", 
    "AREA  :paging #$c_orange :Paging  :STACK", 
//  "AREA  :sleep  #$c_yellow :Sleeping:STACK", 
    );
    break;

case 'freebsd':
default:
    $d = array(
    "DEF:running   =$path/ps_state-running.rrd :value:AVERAGE", 
    "DEF:zombies   =$path/ps_state-zombies.rrd :value:AVERAGE", 
    "DEF:blocked   =$path/ps_state-blocked.rrd :value:AVERAGE", 
    "DEF:stopped   =$path/ps_state-stopped.rrd :value:AVERAGE", 
    "DEF:wait      =$path/ps_state-wait.rrd    :value:AVERAGE", 
    "DEF:idle      =$path/ps_state-idle.rrd    :value:AVERAGE", 
    "DEF:sleeping  =$path/ps_state-sleeping.rrd:value:AVERAGE",

    "CDEF:sleep=sleeping,idle,wait,+,+",

    "AREA  :running#$c_green  :Running ", 
    "AREA  :zombies#$c_red    :Zombies :STACK", 
    "AREA  :blocked#$c_cyan   :Blocked :STACK", 
    "AREA  :stopped#$c_magenta:Stopped :STACK", 
//  "AREA  :sleep  #$c_yellow :Sleeping:STACK", 
    );
    break;
}

rrdgraph(array_merge($g,$d));

?>
