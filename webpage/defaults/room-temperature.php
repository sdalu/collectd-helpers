<?php // $Id$
require $_SERVER['PHPLIB'].'/rrdgraph.php';

rrdgraph(array( 
    "--title=Temperature",
    "--vertical-label=C",
//  '--upper-limit=30',
    '--lower-limit=18',
    '--rigid',

    "DEF:room=$rrdpath/snmp/temperature-room.rrd:value:AVERAGE",

    "LINE:room#$c_red:Room",
)); ?>

