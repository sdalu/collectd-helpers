<?php

$plugin = "snmp";
require_once '../rrdgraph.php';

rrdgraph(array( 
    '--title=CPU',
    '--vertical-label=Percent',
    '--upper-limit=100',
    '--lower-limit=0',
    '--rigid',


    "DEF:user     =$path/cpu-user.rrd     :value:AVERAGE",
    "DEF:nice     =$path/cpu-nice.rrd     :value:AVERAGE",
    "DEF:system   =$path/cpu-system.rrd   :value:AVERAGE",
    "DEF:idle     =$path/cpu-idle.rrd     :value:AVERAGE",
    "DEF:wait     =$path/cpu-wait.rrd     :value:AVERAGE",
    "DEF:kernel   =$path/cpu-kernel.rrd   :value:AVERAGE",
    "DEF:interrupt=$path/cpu-interrupt.rrd:value:AVERAGE",

    'CDEF:total      =user,nice,system,idle,wait,kernel,interrupt,+,+,+,+,+,+',
    'CDEF:user_p     =100,user,     *,total,/',
    'CDEF:nice_p     =100,nice,     *,total,/',
    'CDEF:system_p   =100,system,   *,total,/',
    'CDEF:idle_p     =100,idle,     *,total,/',
    'CDEF:wait_p     =100,wait,     *,total,/',
    'CDEF:kernel_p   =100,kernel,   *,total,/',
    'CDEF:interrupt_p=100,interrupt,*,total,/',

    "AREA:user_p     #$c_yellow :User",
    "AREA:nice_p     #$c_orange :Nice:STACK",
    "AREA:system_p   #$c_red    :System:STACK",
    "AREA:kernel_p   #$c_magenta:Kernel:STACK",
    "AREA:wait_p     #$c_blue   :Wait:STACK",
    "AREA:interrupt_p#$c_cyan   :Interrupt:STACK",
    "AREA:idle_p     #$c_green  :Idle:STACK",
)); ?>
