<?php

$plugin = 'snmp';
require_once '../rrdgraph.php';

rrdgraph(array( 
    '--title=Memory usage',
    '--vertical-label=Bytes',
    '--base=1024',
    '--lower-limit=0',

    "DEF:free_k  =$path/memory-free.rrd  :value:AVERAGE",
    "DEF:cached_k=$path/memory-cached.rrd:value:AVERAGE",
    "DEF:buffer_k=$path/memory-buffer.rrd:value:AVERAGE",
    "DEF:shared_k=$path/memory-shared.rrd:value:AVERAGE",
    "DEF:total_k =$path/memory-total.rrd :value:AVERAGE",
    
    'CDEF:misc_k=total_k,free_k,-,cached_k,-,buffer_k,-,shared_k,-',

    'CDEF:misc  =misc_k,  1024,*',
    'CDEF:free  =free_k,  1024,*',
    'CDEF:cached=cached_k,1024,*',
    'CDEF:buffer=buffer_k,1024,*',
    'CDEF:shared=shared_k,1024,*',

    "AREA:misc  #$c_red   :Used",
    "AREA:shared#$c_orange:Shared:STACK",
    "AREA:cached#$c_yellow:Cached:STACK",
    "AREA:buffer#$c_green :Buffer:STACK",
    "AREA:free  #$c_olive :Free:STACK",
)); ?>
