<?php

$plugin = 'snmp';
require_once '../rrdgraph.php';

// Threads are not reported by UCD MIB
// So Threads = Processes

rrdgraph(array( 
    "--title=Processes",
    "--vertical-label=Count",
    '--lower-limit=0',

    "DEF:processes   =$path/ps_count.rrd:processes:AVERAGE", 
    "DEF:threads     =$path/ps_count.rrd:threads:AVERAGE", 

//  "AREA:threads     #$c_cyan:Thread",
    "AREA:processes   #$c_blue:Process",
)); ?>


