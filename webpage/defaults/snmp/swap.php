<?php

$plugin = 'snmp';
require_once '../rrdgraph.php';

rrdgraph(array( 
    '--title=Swap usage',
    '--vertical-label=Bytes',
    '--lower-limit=0',
    '--base=1024',

    "DEF:free_k =$path/swap-free.rrd :value:AVERAGE",
    "DEF:total_k=$path/swap-total.rrd:value:AVERAGE",
    
    'CDEF:used_k=total_k,free_k,-',

    'CDEF:used=used_k,1024,*',
    'CDEF:free=free_k,1024,*',

    "AREA:used#$c_red  :Allocated",
    "AREA:free#$c_olive:Free:STACK",
)); ?>
