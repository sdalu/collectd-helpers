<?php

$plugin   = 'swap';
require_once '../rrdgraph.php';

rrdgraph(array(
    '--title=Swap usage',
    '--vertical-label=Bytes',
    '--lower-limit=0',
    '--base=1024',

    "DEF:unallocated=$path/swap-free.rrd:value:AVERAGE",
    "DEF:allocated  =$path/swap-used.rrd:value:AVERAGE",

    "AREA:allocated  #$c_red  :Allocated",
    "AREA:unallocated#$c_olive:Free:STACK",
)); ?>

