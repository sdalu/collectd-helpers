<?php

$colors = 'optimum';
require_once '../rrdgraph.php';

$t = isset($title) ? $title : "TCP connections";
$g = array(
    "--title=$t",
    "--vertical-label=Connection",
    "--lower-limit=0",
    "--units-exponent=0",
);

$idx = 0;
foreach($ports as $t => $ap) {
    $ap  = (array) $ap;
    $jdx = 0;
    $l   = array();
    $l_  = array();
    foreach($ap as $p) {
	$p    = rrdgraph_protect($p);
	$f    = "$path/tcpconns-$p-local/tcp_connections-ESTABLISHED.rrd";
	$n    = "p{$idx}_{$jdx}";
	$g[]  = "DEF:$n=$f:value:AVERAGE";
	$l[]  = $n;
	$l_[] = '+';
	$jdx += 1;
    }
    array_shift($l_);
    
    $g[] = "CDEF:p{$idx}=" . join(',', array_merge($l, $l_));

    $t  = rrdgraph_protect($t);
    $c  = $colors[$idx];

    $g[]= "LINE:p{$idx}#$c:$t";
    $idx += 1;
}

rrdgraph($g);

?>
