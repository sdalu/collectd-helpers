<?php

require_once '../rrdgraph.php';

switch($plugin) {
case 'snmp':
     $file = "$hostpath/snmp/users.rrd";
     break;
case 'users':
default:
     $file = "$hostpath/users/users.rrd";
     break;
}

rrdgraph(array( 
    "--title=Users",
    "--vertical-label=User",
    '--lower-limit=0',
    '--upper-limit=5',
    "DEF:users=$file:value:AVERAGE",

    "LINE:users#$c_blue:Logged users",
)); 

?>
