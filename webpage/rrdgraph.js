// $Id: rrdgraph.js 3017 2012-07-14 13:41:15Z sdalu $

//
// Copyright (c)  Stephane D'Alu  2008-2013
// http://www.sdalu.com/
//

// Use: jQuery + Mustache + URI.js

var RRDGraph = (typeof module !== "undefined" && module.exports) || {};

(function (exports) {

exports.name        = "rrdgraph.js";
exports.version     = "0.9.2";
exports.display     = display;
exports.update      = update;
exports.refresh     = refresh;

var tpl = 
  '{{#group}}' 								+
  '<details open="open">' 						+
  '  <summary>{{title}}</summary>' 					+
  '  {{#graph}}' 							+
  '  <figure>' 								+
  '    <figcaption>'							+
  '      <div>{{title}} {{^title}}???{{/title}}</div>'			+
  '      <table>' 							+
  '        {{#labels}}' 						+
  '        <tr><th style="background-color: {{{color}}};">&nbsp;</th>'	+
  '            <td>{{text}}</td></tr>' 					+
  '        {{/labels}}' 						+
  '      </table>' 							+
  '    </figcaption>'							+
  '    <img src="{{>url}}"/>' 						+
  '  </figure>' 							+
  '  {{/graph}}' 							+
  '  <div class="clearfix"/>' 						+
  '</details>' 								+
  '{{/group}}';


function display(id, url, param) {
    if (! (url instanceof URI)) { url = URI(url);			}

    loading(get_hostgraph(url), id)
	.done(function(hostdata) {
	    partial = param ? { url : '{{{url}}}?' + $.param(param) }
                            : { url : '{{{url}}}' };
	    $(id).html($.mustache(tpl, hostdata, partial));
	});
}

function refresh(id) {
    $(id).find('img').each(function() {
        $(this).uri().query(function(data) { data['_'] = $.now(); });
    });
}

function update(id, param) {
    $(id).find('img').each(function() {
	$(this).uri().query($.extend({}, param, { '_' : $.now() }));
    });
}


/* Get host graph information from specified url
 * Returns a promise
 */
var cache_hostgraphs = {};
function get_hostgraph(url, callback) {
    var hg = cache_hostgraphs[url];
    if (!hg || hg.state() == 'rejected') {
	hg = cache_hostgraphs[url] = $.Deferred(function(defer) {
	    $.get(url)
		.then(decode_graphs_xml)
		.then(inject_graph_url.bind(undefined, url))
		.then(inject_description)
		.then(defer.resolve, defer.reject);
	});
    }
    return hg.promise().done(callback);
};


/* Convert XML data to Javascript data
 */
function decode_graphs_xml(xml) {
    var host = $(xml.firstChild);
    return { 
	name : host.attr('name'),
	group: host.find('>group').map(function(i,elm) {
	    return { title: $(elm).attr('title'),
		     graph: $(elm).find('>graph').map(function(i,elm) {
			 return { name: $(elm).attr('name') }; }).get()
		   };
	}).get()
    };
}


/* Add loading information to $(id):
 *  - class         : .loading      and .loading-failed
 *  - text from data:  loading-text and  loading-failed-text
 */
function loading(promise, id) {
    $(id).addClass('loading')
	 .text($(id).data('loading-text'));
    return promise
        .always(function() { $(id).removeClass('loading');		})
	.fail  (function() { $(id).addClass('loading-failed')
			          .text($(id).data('loading-failed')); 	});
}


/* Inject URL for each graph 
 *  based on baseurl and graph name
 */
function inject_graph_url(baseurl, hostdata) {
    return $.Deferred(function(defer) {
	hostdata.group.forEach(function(group) {
	    group.graph.forEach(function(graph) {
		graph.url = baseurl.clone().filename(graph.name + '.php');
	    })
	});
	defer.resolve(hostdata);
    }).promise();
}


/* Inject graph description 
 *   Load the description from the graph URL
 */
function inject_description(hostdata) {
    var defer  = $.Deferred();
    var jqXHRs = [];
    hostdata.group.forEach(function(group) {
	group.graph.forEach(function(graph) {
	    url   = graph.url.clone().query(function(q) { q.describe = null });
	    jqXHR = $.get(url).done(function(resp) { $.extend(graph, resp); });
	    jqXHRs.push(jqXHR);
	})
    });
    $.when.apply($, jqXHRs)
	.done(defer.resolve.bind(undefined, hostdata))
	.fail(defer.reject);
    return defer.promise();
}

})(RRDGraph);



// Local Variables:
// c-indent-level: 4
// c-basic-offset: 4
// End:
