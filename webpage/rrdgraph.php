<?php // Version: 0.5.1

function send_json($data, $callback_varname = 'callback') {
    header("Content-type: application/json");

    if (array_key_exists($callback_varname, $_REQUEST)) {
        echo $_REQUEST[$callback_varname] . '(' . json_encode($data) . ');';
    } else {
        echo json_encode($data);
    }
}


$host        = array_key_exists('host', $_REQUEST)
             ? basename($_REQUEST['host'])
             : basename(dirname($_SERVER['PHP_SELF']));
$rrdpath     = '/var/db/collectd/rrd';
$hostpath    = $rrdpath . '/' . $host;
$script      = basename($_SERVER['PHP_SELF']);
$ostype      = null;
$prefix      = null;

if (preg_match('/^(?:(?<ostype>\w+):)?(?:(?<prefix>[-\w]+)\.)?/', $script, $m)) {
    $ostype  = @$m['ostype'];
    $prefix  = @$m['prefix'];
}

$c_red       = 'FF0000';
$c_green     = '00E000';
$c_olive     = '008000';
$c_blue      = '0000F0';
$c_yellow    = 'F0F000';
$c_orange    = 'F09000';
$c_cyan      = '00A0FF';
$c_magenta   = 'A000FF';
$c_red_h     = 'F7B7B7';
$c_green_h   = 'B7EFB7';
$c_blue_h    = 'B7B7F7';
$c_yellow_h  = 'F3DFB7';
$c_cyan_h    = 'B7DFF7';
$c_magenta_h = 'DFB7F7';


$rrdgraph_colors = array();

// standard
$rrdgraph_colors['standard'] = array(
  '00e000', '0000f0', 'ff0000', '008000',
  'f0f000', 'f09000', '00a0ff', 'a000ff',
  'f7b7b7', 'b7efb7', 'b7b7f7', 'f3dfb7',
  'b7dff7', 'dfb7f7', '000000', 'ffffff',
);

// http://en.wikipedia.org/wiki/Web_colors
$rrdgraph_colors['vga'] = array(
  'c0c0c0', '808000', '808080', '008080',
  '008000', 'ff00ff', '800000', 'ff0000',
  '000080', '800080', '000000', '0000ff',
  '00ffff', '00ff00', 'ffff00', 'ffffff', // Gray
);

// http://ethanschoonover.com/solarized
$rrdgraph_colors['solarized'] = array(
  'b58900', 'cb4b16', 'dc322f', 'd33682', // Colors
  '6c71c4', '268bd2', '2aa198', '859900', // Colors
  '002b36', '073642', '586e75', '657b83', // Base
  '839496', '93a1a1', 'eee8d5', 'fdf6e3', // Base
);

// http://androidarts.com/palette/16pal.htm
$rrdgraph_colors['generic16'] = array(
  'be2633', 'e06f8b', '493c2b', 'a46422',
  'eb8931', 'f7e26b', '2f484e', '44891a',
  'a3ce27', '1b2632', '005784', '31a2f2',
  'b2dcef', '000000', '9d9d9d', 'ffffff',
);

// http://web.media.mit.edu/~wad/color/palette.html
$rrdgraph_colors['optimum'] = array(
  '2a4bd7', '1d6914', '814a19', '8126c0',
  '9dafff', '81c57a', 'e9debb', 'ad2323',
  '29d0d0', 'ffee33', 'ff9233', 'ffcdf3',
  '000000', '575757', 'a0a0a0', 'ffffff',
);

// http://www.iges.org/grads/gadoc/16colors.html
$rrdgraph_colors['grads'] = array(
  'fa3c3c', '00dc00', '1e3cff', '00c8c8',
  'f00082', 'e6dc32', 'f08228', 'a000c8',
  'a0e632', '00a0ff', 'e6af2d', '00d28c',
  '8200dc', 'aaaaaa', '000000', 'ffffff',
);

$rrdgraph_colors['grads-rainbow'] = array(
  'a000c8', '6e00dc', '1e3cff', '00a0ff',
  '00c8c8', '00d28c', '00dc00', 'a0e632',
  'e6dc32', 'e6af2d', 'f08228', 'fa3c3c',
  'f00082', 'aaaaaa', '000000', 'ffffff',
);


// Select default color set
if (isset($colors)) {
   if (is_string($colors))
       $colors = $rrdgraph_colors[$colors];
} else {
    $colors = $rrdgraph_colors['standard'];
}



$path = $hostpath;
if (isset($plugin) && !empty($plugin)) {
   $path = $plugpath = "$hostpath/$plugin";
   if (isset($instance) && (is_numeric($instance) || !empty($instance)))
       $path = $plugpath = "$plugpath-$instance";

   if (isset($type) && !empty($type))
       $path = $typepath = "$plugpath/$type";
}

function rrdgraph_protect($txt) {
    return preg_replace('/[:\\\\]/', '\\\${0}', $txt);
}


function rrdgraph_normalize($opts) {
    $r = array();
    foreach($opts as $o) {
	if (preg_match('/^\s*--/', $o)) {
	    $r[] = $o;
	} elseif (preg_match('/^\s*(?<op>[^:]+?)\s*:/', $o, $m)) {
	    $protect   = array();
	    switch($m['op']) {
	    case 'TICK' :
		$protect[3] = null;
		break;
	    case 'HRULE': case 'VRULE':
	    case 'LINE' : case 'LINE1':	case 'LINE2': case 'LINE3':
	    case 'AREA' :
		$protect[2] = null;
		break;
	    case 'PRINT': case 'GPRINT':
		$protect[2] = null;
	    case 'COMMENT':
		$protect[1] = null;
		break;
	    case 'DEF'  : case 'CDEF'  : case 'VDEF' :
	    default:
	    }
	    if (empty($protect)) {
		$r[] = preg_replace('/\s+/', '', $o);
	    } else {
		$a = explode(':', $o);
		foreach($protect as $i => $p)
		    if (array_key_exists($i, $a))
			$protect[$i] = $a[$i];
		$a = preg_replace('/\s+/', '', $a);
		foreach($protect as $i => $p) 
		    if (array_key_exists($i, $a))
			$a[$i] = $p;
		$r[] = join(':', $a);
	    }
	} else {
	    $r[] = $o;
	}
    }
    return $r;
}


function rrdgraph_describe($opts) {
    $desc   = array();

    /* Labels 
     */
    $labels = array();
    $rrd_labels = preg_grep('/^(?:LINE.?|AREA):(.*)/', $opts);
    foreach($rrd_labels as $l) {
        if (!preg_match('/^\w+:\w+(#[^:]+):(.*?)(?::STACK)?$/', $l, $m))
	    continue;
	$labels[] = array('color' => $m[1], 
		    	  'text' => preg_replace('/\\\\[lgncr]$/', '', $m[2]));
    }
    if (! empty($labels))
        $desc['labels'] = $labels;

    /* Title
     */
    if (($titles = preg_grep('/^(?:-t|--title)[=\s](.*)$/', $opts))) {
	$titles = array_values($titles);
        preg_match('/^[^=\s]+[=\s](.*)/', $titles[0], $m);
	$desc['title'] = $m[1];
    }

    /* Axes
     */
    if (($axes = preg_grep('/^(?:-v|--vertical-label)[=\s](.*)$/', $opts))) {
	$axes  = array_values($axes);
        preg_match('/^[^=\s]+[=\s](.*)/', $axes[0], $m);
	$desc['y'] = $m[1];
    }

    send_json($desc);
}


function rrdgraph($opts) {
    $opts = rrdgraph_normalize($opts);
    $mime = 'image/png';

    if (array_key_exists('describe', $_REQUEST))
        return rrdgraph_describe($opts);

    if (array_key_exists('notitle', $_REQUEST))
        $opts = preg_grep('/^(:?--title|-t)[=\s]/', $opts, PREG_GREP_INVERT);

    $opts = array_values($opts);

    array_unshift($opts, '-E');
    array_unshift($opts, '--disable-rrdtool-tag');

    if (array_key_exists('transparent', $_REQUEST)) {
        array_unshift($opts, '--color=BACK#F2F2F200');
        array_unshift($opts, '--color=CANVAS#FFFFFF00');	
        array_unshift($opts, '--color=SHADEA#CECECE00');
        array_unshift($opts, '--color=SHADEB#9E9E9E00');
    }

    if (array_key_exists('nolegend', $_REQUEST)) {
        array_unshift($opts, '--no-legend');
        $opts = preg_grep('/^(:?--vertical-label|-v)[=\s]/', $opts,
	                  PREG_GREP_INVERT);
    }

    if (array_key_exists('imgformat', $_REQUEST)) {
	switch (strtolower($_REQUEST['imgformat'])) {
	case 'svg': 
	    array_unshift($opts, '--imgformat', 'SVG');
	    $mime = 'image/svg';
	    break;
	}
    }

    if (array_key_exists('width', $_REQUEST))
        array_unshift($opts, '--width', $_REQUEST['width']);

    if (array_key_exists('height', $_REQUEST))
        array_unshift($opts, '--height', $_REQUEST['height']);

    if (array_key_exists('onlygraph', $_REQUEST))
        array_unshift($opts, '--only-graph');

    if (array_key_exists('start', $_REQUEST))
        array_unshift($opts, '--start', $_REQUEST['start']);

    if (array_key_exists('xgrid', $_REQUEST))
        array_unshift($opts, '--x-grid', $_REQUEST['xgrid']);

    $f   = tempnam("/tmp", "rrd");
    $ret = rrd_graph($f, $opts);
    if( $ret === false ) {
	$err = rrd_error();
	echo "rrd_graph() ERROR: $err\n";
    } else {
	header("Content-type: $mime");
	readfile($f);
    }
    unlink($f);

}

?>
